import java.util.ArrayList;

class Task15{
    public static void main(String[]args){
        if(args.length > 0){
            ArrayList<Person> contacts = new ArrayList<Person>();
            ArrayList<Person> results = new ArrayList<Person>();

            Person clark = new Person();
            Person bruce = new Person("Bruce","Wayne","--");
            Person jack = new Person("Jack","Reacher","0022442");
            Person will = new Person("Will","Smith","34343434");
            Person john = new Person("John","Wick","34224343");

            contacts.add(clark);
            contacts.add(bruce);
            contacts.add(jack);
            contacts.add(will);
            contacts.add(john);
            System.out.println("Searching for "+args[0]);
            searchContacts(args[0],results, contacts);
        }else{
            throw new IllegalArgumentException("Need at least one argument to search for a contact!");
        }
    }

    public static void searchContacts(String nameSearch, ArrayList<Person> results, ArrayList<Person> list){
        for(Person s: list){
            if(s.getName().toLowerCase().contains(nameSearch.toLowerCase())){
                results.add(s);
            }
        }

        if(results.size() > 0){
            for (Person p : results) {
                System.out.println("Found "+p.firstName+" "+p.lastName);
            }
        }
    }
}