import java.util.ArrayList;

class Person {
    String firstName;
    String lastName;
    String phone;

    public Person(){
        this.firstName = "Clark";
        this.lastName = "Kent";
        this.phone="4553343";
    }

    public Person(String firstName, String lastName,String phone){
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }

    public String getName(){
        return firstName+" "+lastName;
    }
}